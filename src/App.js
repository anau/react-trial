import React, { useState, Component } from 'react';
import './App.css';
import { Layout, Avatar, Menu, Icon, Breadcrumb, Button, Carousel, Card, Tabs, Drawer } from 'antd';
import axios from 'axios';
import Title from 'antd/lib/typography/Title';
import SubMenu from 'antd/lib/menu/SubMenu';
var imgUrl="http://image.tmdb.org/t/p/w185//";

class App extends Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  constructor(){
    super()

    this.state={
      movies:[]
    }

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event){
    event.preventDefault();
    var query = this.input.value;
    console.log(query);
    this.componentDidMount(query);
  }
  componentDidMount(query){
    var api = 'https://api.themoviedb.org/3/search/movie?api_key=98e29b7ce43f0e1d5ee589664b968f3e&query='
    axios.get(api + query)
      .then(response => 
        this.setState ({
          movies:response.data.results
        }))
  }

      render() {
        const { Meta } = Card;
       const contentStyle = {
        height: '240px', 
        width : '400px', 
        marginLeft: '30%'  
      }
      function callback(key) {
        console.log(key);
      }

      const { Header, Footer, Sider, Content } = Layout;
      function onChange(a, b, c) {
        console.log(a, b, c);
      }
      const { TabPane } = Tabs;
      const tabstyle ={
        color: '#000' ,
        float: 'center' ,
      }
        const {movies} = this.state;
        var movieList = movies.map((movie) => 
        <div className="col-4 movie" style={{backgroundColor: "transparent"}}>  
          <img style={{transform:"translate(-50%)", marginLeft: '50%'}} src={imgUrl + movie.poster_path}className="movieImg" />
          <p className="overview">{movie.overview}</p>
          <h3  key={movie.id}className="text-center movieTitle">{movie.title}</h3>
        </div>)  
  return (
    <div className="App">
      <Layout>
        <Header className="header" style={{display: 'flex', justifyContent: 'space-between'}}> 
          <Title style={{ color: 'white' }} level={3}>MOVIE</Title>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">MOVIE</Menu.Item>
            <Menu.Item key="2">STUDIO</Menu.Item>
            <Menu.Item key="3">JADWAL</Menu.Item>
          </Menu>
        </Header>
        <Layout>
          <Sider>
            <Menu
              defaultSelectedKeys={['Dashboard']}
              mode="inline"
            >
              <Menu.Item key='Dashboard'>
                Dashboard
            </Menu.Item>
              <SubMenu
                title={
                  <span>
                    <Icon type="mail" />
                    <span>About US</span>
                  </span>
                }
              >
                <Menu.ItemGroup key='AboutUS' title='Country 1'>
                  <Menu.Item key='location1'> Location 1</Menu.Item>
                  <Menu.Item key='location2'> Location 2</Menu.Item>
                </Menu.ItemGroup>
              </SubMenu>
              <Menu.Item key='Movie'>
                <a href="https://www.google.com/"/>
                Movie
            </Menu.Item>
            <Menu.Item key='ComingSoon'>
                Coming soon
            </Menu.Item>
            <Menu.Item key='Time'>
                Time
            </Menu.Item>
            <Menu.Item key='Promotion'>
                Promotion
            </Menu.Item>
            <Menu.Item key='EndYear'>
                EndeYear Movie
            </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Content style={{ padding: '0 50px' }}>
              <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
              </Breadcrumb>
              <Carousel autoplay>
                <div>
                  <img style={contentStyle} src='./diskon1.jpg'/>
                </div>
                <div>
                <img style={contentStyle} src='./diskon2.jpg'/>
                </div>
                <div>
                <img style={contentStyle} src='./diskon3.png'/>
                </div>
                <div>
                <img style={contentStyle} src='./diskon4.jpg'/>
                </div>
              </Carousel>
              <div style={{backgroundColor:'transparent'}} className="jumbotron">  
                <div className="container">
                <div className="row">
                <h2 className="col-12 text-center">Search for a Movie</h2>
                  <form onSubmit={this.onSubmit} className="col-12">
                    <input className= "col-12 form-control" placeholder="Search Movies..."
                    ref = {input => this.input = input}/>
                  </form>
                  <div>
                    <ul className= "col-12 row">{movieList}</ul>
                  </div>
                  </div>
                </div>
              </div>
              {/* <Tabs defaultActiveKey="1" onChange={callback} style={tabstyle} centered>
                <TabPane tab="Page 1" key="1">
                  <div style={{ background: '#fff', padding: 24, display: "flex"}}>
                    <Card
                      hoverable
                      style={{ width: 240 , marginLeft: 25}}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>
                        MORE INFO
                      </Button>
                      
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240, marginLeft: 25 }}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240 , marginLeft: 25}}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240, marginLeft: 25 }}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                </div>
                <div style={{ background: '#fff', padding: 24, display: "flex"}}>
                    <Card
                      hoverable
                      style={{ width: 240 , marginLeft: 25}}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>
                        MORE INFO
                      </Button>
                      
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240, marginLeft: 25 }}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240 , marginLeft: 25}}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240, marginLeft: 25 }}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                </div>
               
                </TabPane>
                <TabPane tab="Page 2" key="2">
                <div style={{ background: '#fff', padding: 24, display: "flex"}}>
                    <Card
                      hoverable
                      style={{ width: 240 , marginLeft: 25}}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>
                        MORE INFO
                      </Button>
                      
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240, marginLeft: 25 }}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240 , marginLeft: 25}}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                    <Card
                      hoverable
                      style={{ width: 240, marginLeft: 25 }}
                      cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                    >
                      <Meta title="Europe Street beat" />
                      <Button style={{marginTop: 10}} onClick={this.showDrawer}>MORE INFO</Button>
                    </Card>
                </div>
                </TabPane>
              </Tabs> */}
              {/* <Drawer
                width={700}
                title="MOVIE DETAILS"
                placement="right"
                closable={false}
                onClose={this.onClose}
                 visible={this.state.visible}
              >
                <img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
                <p>TITLE = Europe Street beat</p>
                <p>TIME = 10.00 WIB</p>
              </Drawer> */}
            </Content>
          
            <Footer style={{ textAlign: 'center' }}>MOVIE</Footer>
          </Layout>
        </Layout>
      </Layout>
    </div>
  );
}
}



export default App;
